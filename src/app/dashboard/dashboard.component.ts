import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  fileContent: String = "";
  fileType:string="";
  public totalArr : Array<any> = [];
  // function to check the username and password
  public onChange(fileList: FileList): void {
    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    let self = this;
    fileReader.onloadend = function(x) {
      const csv: string | ArrayBuffer = fileReader.result;
    }
     let fileName  = fileReader.readAsText(file);
      console.log(file.type); 
      var vm = this;
      vm.fileType = file.type;
      this.totalArr.push(file.type);
      console.log(this.totalArr);
      localStorage.setItem('fileType', JSON.stringify(this.totalArr));
  }

  Download(){
    alert("download button works");
  }

  constructor() { }

  ngOnInit() {
  }

}
