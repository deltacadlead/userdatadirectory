import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component'
import { AuthGuardService } from './services/auth-guard.service';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {path:'',redirectTo:'/login',pathMatch: 'full' },
  {path:'dashboard',component:DashboardComponent,canActivate:[AuthGuardService]},
  {path:'login',component:LoginComponent,canActivate:[AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
